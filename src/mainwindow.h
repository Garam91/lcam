#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "videodevice.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <QSpinBox>
#include <QCheckBox>
#include <opencv2/opencv.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    std::vector<camera_info> cam_list;
    VideoDevice cam;
    std::string device_name;
    std::string pix_fmt;
    int width, height;

    std::thread* preview_thread;
    std::mutex preview_mutex;
    std::condition_variable preview_con;
    QImage* frame_qimg;
    cv::Mat* frame_mat;
    cv::VideoWriter video_writer;
    bool preview;
    int period;
    uint fps;
    bool img_save;
    bool video_save;

    std::map<uint, QSpinBox*> spinbox_map;
    std::map<uint, QCheckBox*> checkbox_map;

    void Capture();
    void SettingWidget_SetEnabled(bool enable);

private slots:
    void frame_update();

    void on_pushButton_open_clicked();
    void on_pushButton_close_clicked();

    void on_pushButton_browse_clicked();
    void on_pushButton_save_img_clicked();
    void on_pushButton_record_start_clicked();
    void on_pushButton_record_stop_clicked();

    void on_comboBox_resolution_activated(int index);
    void on_comboBox_color_space_activated(int index);
    void on_spinBox_brightness_valueChanged(int arg1);
    void on_spinBox_constrast_valueChanged(int arg1);
    void on_checkBox_auto_ex_priority_toggled(bool checked);
    void on_checkBox_auto_ex_toggled(bool checked);
    void on_spinBox_ex_valueChanged(int arg1);
    void on_spinBox_focus_valueChanged(int arg1);
    void on_spinBox_gain_valueChanged(int arg1);
    void on_spinBox_gamma_valueChanged(int arg1);
    void on_spinBox_hue_valueChanged(int arg1);
    void on_spinBox_pan_valueChanged(int arg1);
    void on_spinBox_saturation_valueChanged(int arg1);
    void on_spinBox_sharpness_valueChanged(int arg1);
    void on_spinBox_tilt_valueChanged(int arg1);
    void on_checkBox_auto_wb_toggled(bool checked);
    void on_spinBox_wb_valueChanged(int arg1);
    void on_spinBox_zoom_valueChanged(int arg1);

protected:
    virtual void showEvent(QShowEvent *event);

signals:
    void frame_capture();
};

#endif // MAINWINDOW_H
