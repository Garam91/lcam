#ifndef VIDEODEVICE_H
#define VIDEODEVICE_H

#include <iostream>
#include <linux/videodev2.h>
#include <libv4lconvert.h>
#include <vector>
#include <uchar.h>

typedef struct _cam_format{
    v4l2_fmtdesc fmt_desc;
    std::string pixel_format;
    std::string buf_type;
}cam_format;

typedef struct _buffer{
    void*   data;
    size_t  size;
}buffer;

typedef struct _camera_info{
  std::string name;
  v4l2_capability cap;
}camera_info;

class VideoDevice
{
public:
    std::string error_msg;
    std::vector<cam_format> format_list;
    std::vector<v4l2_frmsize_discrete> frame_size_list;
    std::vector<v4l2_queryctrl> control_list;

    /**
     * @brief 생성자
	 * @details 카메라와 버퍼의 정보 초기화
     */
    VideoDevice();

    /**
     * @brief 소멸자. 
	 * @details 카메라 정보와 버퍼 메모리 해제
     */
    ~VideoDevice();

    /**
     * @brief 카메라 연결
     * @param device_name 경로를 포함한 카메라의 디바이스 파일 이름. 예)/dev/video0
     * @return 성공 여부 반환
     */
    bool Open(const char* device_name);

    /**
     * @brief 디바이스 파일 연결 해제
     */
    void Close();

    /**
     * @brief 카메라 해상도 설정
     * @param width 해상도 가로
     * @param height 해상도 세로
     * @return 성공 여부 반환
     */
    bool SetRsolution(uint width, uint height);

    /**
     * @brief 카메라의 픽셀 포맷 설정(기본 포맷)
     * @return 성공 여부 반환
     */
    bool SetFormat();

    /**
     * @brief 픽셀 포맷 설정
     * @param pixel_fmt 설정할 픽셀 포맷
     * @return 성공 여부 반환
     */
    bool SetFormat(__u32 pixel_fmt);

    /**
     * @brief 픽셀 모팻과 해상도 설정
     * @param pixel_fmt 설정할 픽셀 포맷
     * @param w 설정할 가로 해상도
     * @param h 설정할 세로 해상도
     * @return 성공 여부 반환
     */
    bool SetFormat(__u32 pixel_fmt, __u32 w, __u32 h);

    /**
     * @brief 카메라 버퍼 할당
     * @param buf_count 카메라가 사용할 버퍼의 수
     * @return 
     */
    bool PrepareBuffer(__u32 buf_count = 4);

    /**
     * @brief 버퍼 해제
     */
    void FreeBuffer();

    /**
     * @brief 카메라 촬영 시작
     * @return 성공 여부 반환
     */
    bool StreamOn();

    /**
     * @brief 카메라 촬영 종료
     * @return 성공 여부 반환
     */
    bool StreamOff();

    /**
     * @brief 카메라가 촬영한 프레임 데이터 복사
     * @param data 프레임 데이터를 복사할 주소값
     * @param format 복사하는 프레임 데이터의 색상 포맷. 예) V4L2_PIX_FMT_GREY, V4L2_PIX_FMT_RGB24 ...
     * @return 프레임 데이터의 크기(byte단위). 촬영 실패 시에는 0을 반환.
     */
    uint Capture(unsigned char* data, __u32 format = V4L2_PIX_FMT_RGB24);

    /**
     * @brief 현재 연결된 카메라의 디바이스 파일 이름 반환
     * @return 연결된 카메라의 디바이스 파일 이름(/dev/video*)
     */
    std::string GetName(){return device_name;}

	/**
     * @brief 카메라 디바이스의 포맷 반환
	 * @return 카메라 디바이스의 포맷 반환
	 */
    std::string GetFormat(){return pixfmt_to_str(src_fmt.fmt.pix.pixelformat);}

    /**
     * @brief 가로 해상도 반환
     * @return 현재 가로 해상도
     */
    uint GetWidth(){return src_fmt.fmt.pix.width;}

    /**
     * @brief 세로 해상도 반환
     * @return 현재 세로 해상도
     */
    uint GetHeight(){return src_fmt.fmt.pix.height;}

    /**
     * @brief 카메라 파라미터 설정
     * @param id 설정할 파라미터의 id
     * @param value 설정할 값
     * @return
     */
    bool SetControl(__u32 id, __s32 value);

    /**
     * @brief 현재 카메라 파라미터의 설정값 반환
     * @param id 파라미터의 id
     * @param value 현재 설정된 값
     */
    void GetControl(__u32 id, __s32* value);

    /**
     * @brief 카메라의 지원 포맷 리스트를 format_list에 저장
     * @return 성공 여부 반환
     */
    bool GetFormatList();

    /**
     * @brief 카메라의 지원 해상도
     * @return 성공 여부 반환
     */
    bool GetResolutionList();

    /**
     * @brief 카메라의 설정 가능한 파라미터 리스트
     */
    void GetControlsList();

    /**
     * @brief 현재 연결된 카메라 리스트 반환
     * @param device_list 카메라 리스트
     * @return 연결된 카메라 갯수.
     */
    static uint GetDeviceList(std::vector<camera_info>& device_list);

    /**
     * @brief 현재 프레임 레이트 바환
     * @return 프레임 레이트
     */
    uint GetFps();

private:
    /*Information of current device*/
    std::string device_name;
    int fd;
    bool opened;
    __u32 buf_type;
    __u32 buf_cnt;
    v4l2_capability capability;
    struct v4l2_format src_fmt;
    struct v4l2_format dst_fmt;
    struct v4lconvert_data* convert_data;
    buffer* buffers;
    unsigned char* frame_data;

    bool open_device();
    void close_device();

    bool ioctl(unsigned int code, void* arg, bool error_msg = true);
    bool g_fmt(unsigned type, v4l2_format &fmt);
    bool try_fmt(v4l2_format &fmt);
    bool s_fmt(v4l2_format &fmt);
    bool reqbufs(v4l2_requestbuffers &req_buf, __u32 buf_type, int count);
    bool enum_fmt(v4l2_fmtdesc &fmt, unsigned type, int index = 0);
    std::string pixfmt_to_str(__u32 id);
    std::string buftype_to_str(__u32 id);

    /**
     * @brief 현재 프레임의 데이터를 'frame_data'변수에 저장
     * @return 성공 여부 반환
     */
    bool get_frame();

    /**
     * @brief 픽셀 포맷 변환
     * @param dst_data 변환 후 데이터를 복사할 주소
     * @param dst_format 변환할 포맷. 예) V4L2_PIX_FMT_BGR24, V4L2_PIX_FMT_GREY, ....
     * @return 변환 후 데이터 크기(byte단위). 0을 반환 시, 변환 실패.
     */
    uint convert_format(void* dst_data, __u32 dst_format);
};

#endif // VIDEODEVICE_H
