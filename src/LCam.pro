QT += core gui widgets

CONFIG += c++11

HEADERS += \
    mainwindow.h \
    videodevice.h

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    videodevice.cpp

LIBS += -lv4l2 -lv4lconvert `pkg-config opencv --libs`

FORMS += \
    mainwindow.ui

RESOURCES += \
    resource.qrc

CONFIG(debug, debug|release):DEFINES+=DEBUG
CONFIG(release, debug|release):DEFINES+=RELEASE
