#include "videodevice.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <unistd.h>
#include <cstring>
#include <cassert>
#include <sys/io.h>
#include <dirent.h>
#include <algorithm>

#define BUFFER_COUNT        4
#define BUFFER_TYPE         V4L2_BUF_TYPE_VIDEO_CAPTURE

/* public function */

VideoDevice::VideoDevice()
    :fd(-1), opened(false),
      buf_type(BUFFER_TYPE), buf_cnt(BUFFER_COUNT)
{
    device_name.clear();
}

VideoDevice::~VideoDevice()
{
    Close();
}

bool VideoDevice::Open(const char* device_name)
{
    this->device_name = device_name;
    struct stat st;

    /*Check current status of the device file*/
    if( stat(device_name, &st) == -1 ){//open fail
        /*Save error message*/
        error_msg = "'" + this->device_name + "'" + " cannot identify!, "
                + std::to_string(errno) + " : " + strerror(errno);
        std::cerr<<error_msg<<std::endl;

        /*clear device information*/
        this->device_name.clear();
        opened = false;

        return false;
    }else{//success
        opened = open_device();

        return opened;
    }
}

void VideoDevice::Close()
{
    if(opened){
        close_device();
        opened = false;

        format_list.clear();
        frame_size_list.clear();
        control_list.clear();
    }
}

bool VideoDevice::SetRsolution(uint width, uint height)
{
    /*Get current format*/
    g_fmt(buf_type, src_fmt);

    /*Input resolution to src_fmt*/
    src_fmt.fmt.pix.width = width;
    src_fmt.fmt.pix.height = height;

    /*Set format*/
    try_fmt(src_fmt);
    return s_fmt(src_fmt);
}

bool VideoDevice::SetFormat()
{
    /*Get current format*/
    g_fmt(buf_type, src_fmt);

    /*Set current format*/
    try_fmt(src_fmt);
    if(!s_fmt(src_fmt)){
        error_msg = strerror(errno);//save error message
        return false;
    }

    return true;
}

bool VideoDevice::SetFormat(__u32 pixel_fmt)
{
    /*Get current format*/
    g_fmt(buf_type, src_fmt);

    /*Input value to format*/
    src_fmt.fmt.pix.pixelformat = pixel_fmt;

    /*Set current format*/
    try_fmt(src_fmt);
    if(!s_fmt(src_fmt)){
        error_msg = strerror(errno);//save error message
        return false;
    }

    return true;
}

bool VideoDevice::SetFormat(__u32 pixel_fmt, __u32 w, __u32 h)
{
    /*Get current format*/
    g_fmt(buf_type, src_fmt);

    /*Input value to format*/
    src_fmt.fmt.pix.pixelformat = pixel_fmt;
    src_fmt.fmt.pix.width = w;
    src_fmt.fmt.pix.height = h;

    /*Set current format*/
    try_fmt(src_fmt);
    if(!s_fmt(src_fmt)){
        error_msg = strerror(errno);//save error message
        return false;
    }

    return true;
}

bool VideoDevice::PrepareBuffer(__u32 buf_count)
{
    if(buf_count < 2){
        error_msg = "buffer counter is less than 4 ";
        return false;
    }

    /*Request buffers*/
    struct v4l2_requestbuffers reqbuf;
    if(!reqbufs(reqbuf, buf_type, buf_count)){
        error_msg = "VIDIOC_REQBUFS failed : ";
        error_msg += strerror(errno);
        std::cerr<<error_msg<<std::endl;
        return false;
    }

    buf_cnt = buf_count;

    /*Dynamic allocation of buffer memory*/
    buffers = (buffer*)calloc(reqbuf.count, sizeof(*buffers));
    frame_data = (unsigned char*)calloc(
                src_fmt.fmt.pix.sizeimage, sizeof(unsigned char));

    /*struct v4l2_buffer clear*/
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));

    /*Query buffer and Memory mapping*/
    buf.type = buf_type;
    buf.memory = V4L2_MEMORY_MMAP;

    for(int buf_num = 0 ; buf_num < (int)reqbuf.count ; buf_num++){
        if(!ioctl(VIDIOC_QUERYBUF, &buf)){
            return false;
        }

        buf.index = buf_num;
        buffers[buf_num].size = buf.length;
        buffers[buf_num].data = mmap(NULL, buf.length, PROT_READ | PROT_WRITE,
                                     MAP_SHARED, fd, buf.m.offset);

        if (buffers[buf_num].data == MAP_FAILED){
            error_msg = "mmap failed.(buffer " + std::to_string(buf.index) + ")";
            std::cerr<<error_msg<<std::endl;
            return false;
        }
    }

    /*Ready to pixel format conversion*/
    dst_fmt = src_fmt;
    convert_data = v4lconvert_create(fd);

    return true;
}

void VideoDevice::FreeBuffer()
{
    /*Memory unmapping*/
    for (int index = 0 ; index < buf_cnt ; index++){
        if(munmap(buffers[index].data, buffers[index].size) == -1){
            error_msg = "Free buffers failed";
        }
    }

    /*Release buffers*/
    free(buffers);
    v4lconvert_destroy(convert_data);
    free(frame_data);
}

bool VideoDevice::StreamOn()
{
    /*Queue buffer*/
    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));
    buf.type = buf_type;
    buf.memory = V4L2_MEMORY_MMAP;

    for (int index = 0 ; index < buf_cnt ; index++) {
        buf.index = index;

        if (!ioctl(VIDIOC_QBUF, &buf)){
            return false;
        }
    }

    /*Stream on*/
    return ioctl(VIDIOC_STREAMON, &buf_type);
}

bool VideoDevice::StreamOff()
{
    /*Stream off*/
    return ioctl(VIDIOC_STREAMOFF, &buf_type);
}

uint VideoDevice::Capture(unsigned char* data, __u32 format)
{
    if(get_frame()){
        return convert_format(data, format);
    }
}

bool VideoDevice::SetControl(__u32 id, __s32 value)
{
    struct v4l2_control ctrl;
    memset(&ctrl, 0, sizeof(ctrl));
    ctrl.id = id;
    ctrl.value = value;

    if(!ioctl(VIDIOC_S_CTRL, &ctrl))
        return false;

    return true;
}

void VideoDevice::GetControl(__u32 id, __s32* value)
{
    struct v4l2_control ctrl;
    memset(&ctrl, 0, sizeof(ctrl));
    ctrl.id = id;

    if(!ioctl(VIDIOC_G_CTRL, &ctrl))
        *value = -1;
    else
        *value = ctrl.value;
}

bool VideoDevice::GetFormatList()
{
    cam_format fmt;
    format_list.clear();
    int format_index;

    /*Get supported format*/
    for(format_index = 0 ; ; format_index++){

        if(!enum_fmt(fmt.fmt_desc, buf_type, format_index))
            break;

        /*convert pixel format and buffer type to string*/
        fmt.buf_type = buftype_to_str(fmt.fmt_desc.type);
        fmt.pixel_format = pixfmt_to_str(fmt.fmt_desc.pixelformat);

        format_list.push_back(fmt);
    }

    return format_index > 0;
}

bool VideoDevice::GetResolutionList()
{
    v4l2_format fmt;
    v4l2_frmsizeenum frm_size;
    int frame_index;

    frame_size_list.clear();

    /*Get frame size*/
    g_fmt(buf_type, fmt);

    for(frame_index = 0 ; ; frame_index++){
        memset(&frm_size, 0, sizeof(frm_size));
        frm_size.pixel_format = fmt.fmt.pix.pixelformat;
        frm_size.index = frame_index;

        if(!ioctl(VIDIOC_ENUM_FRAMESIZES, &frm_size, false))
            break;

        frame_size_list.push_back(frm_size.discrete);
    }

    return frame_index > 0;
}

void VideoDevice::GetControlsList()
{
    v4l2_queryctrl query_ctrl;

    control_list.clear();
    query_ctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;
    do{
        /*Get control information*/
        if(!ioctl(VIDIOC_QUERYCTRL, &query_ctrl, false))
            break;

        control_list.push_back(query_ctrl);//input control information

        query_ctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;//next
    }while(true);
}

uint VideoDevice::GetDeviceList(std::vector<camera_info>& device_list)
{
    int i;
    std::string path = "/sys/class/video4linux/";
    DIR *dir;
    struct dirent *ent;
    int file_cnt = 0;
    std::vector<int> node_list;
    std::vector<std::string> file_list;
    char temp[5];
    std::string fn;
    int fd;
    camera_info info;

    /*check v4l2 directory*/
    if(access(path.c_str(), F_OK) == -1){
        std::cerr<<"No surch '"<<path<<"' directory"<<std::endl;
        return 0;
    }

    /*Get video file list*/
    dir = opendir(path.c_str());//open directory
    if(dir != NULL){
        node_list.clear();
        /*check video files*/
        while ((ent = readdir(dir)) != NULL){
            std::memcpy(temp, ent->d_name, 5);
            if(std::strcmp("video", temp) == 0){
                temp[0]=ent->d_name[5];
                temp[1]='\0';
                node_list.push_back(std::atoi(temp));
            }
        }
        file_cnt = node_list.size();
        closedir(dir);

        if(file_cnt > 0){
            /*List sort*/
            std::sort(node_list.begin(), node_list.end());
            for(i=0;i<file_cnt;i++)
                file_list.push_back("video"+std::to_string(node_list[i]));
        }else if(file_cnt == 0)
            return 0;
#ifdef DEBUG
        for(i=0;i<file_cnt;i++)std::cout<<file_list[i]<<std::endl;
#endif
    }

    /*Get camera information*/
    device_list.clear();
    for(i = 0 ; i < node_list.size() ; i++) {
        /*Open file*/
        fn = "/dev/" + file_list[i];
        fd = open(fn.c_str(), O_RDWR | O_NONBLOCK);
        if (fd < 0)
            continue;

        /*Obtain information of device and driver*/
        memset(&info.cap, 0, sizeof(info.cap));
        if(::ioctl(fd, VIDIOC_QUERYCAP, &info.cap) == -1){
            close(fd);
            continue;
        }

        /*Check metadata capture device.
         *Metadevices are excluded from the list*/
        if(!(info.cap.device_caps & V4L2_CAP_META_CAPTURE)){
            info.name = fn;
            device_list.push_back(info);
        }
        close(fd);
    }

    return device_list.size();
}

uint VideoDevice::GetFps()
{
    struct v4l2_frmivalenum frmival_enum;

    memset(&frmival_enum, 0, sizeof(frmival_enum));
    frmival_enum.pixel_format = src_fmt.fmt.pix.pixelformat;
    frmival_enum.width = src_fmt.fmt.pix.width;
    frmival_enum.height = src_fmt.fmt.pix.height;
    if(!ioctl(VIDIOC_ENUM_FRAMEINTERVALS, &frmival_enum))
        return 0;

    return frmival_enum.discrete.denominator;
}

/* private function */

bool VideoDevice::open_device()
{
    /*Open file*/
    fd = open(device_name.c_str(), O_RDWR | O_NONBLOCK);
    if (fd < 0) {
        error_msg = "Cannot open " + device_name;
        std::cerr<<error_msg<<std::endl;
        return false;
    }

    /*Obtain information of device and driver*/
    memset(&capability, 0, sizeof(capability));
    if(!ioctl(VIDIOC_QUERYCAP, &capability)){
        /*Close file when VIDIOC_QUERYCAP failed*/
        close_device();

        error_msg = device_name + " is not a V4L2 device";
        std::cerr<<error_msg<<std::endl;
        return false;
    }

    return true;
}

void VideoDevice::close_device()
{
    close(fd);
    fd = -1;
}

bool VideoDevice::ioctl(unsigned int code, void *arg, bool error_msg)
{
    int re;

    re = ::ioctl(fd, code, arg);
    if( (error_msg)&&(re == -1) ){
        char msg[100];
        std::sprintf(msg, "ioctl %d error : %s", code, strerror(errno));
        error_msg = msg;
        std::cerr<<error_msg<<std::endl;
    }

    return re == 0;
}

bool VideoDevice::g_fmt(unsigned type, v4l2_format &fmt)
{
    /*Request current format*/
    memset(&fmt, 0, sizeof(fmt));
    fmt.type = type;
    return ioctl(VIDIOC_G_FMT, &fmt);
}

bool VideoDevice::try_fmt(v4l2_format &fmt)
{
    if (V4L2_TYPE_IS_MULTIPLANAR(fmt.type)) {
        fmt.fmt.pix_mp.plane_fmt[0].bytesperline = 0;
        fmt.fmt.pix_mp.plane_fmt[1].bytesperline = 0;
    }else{
        fmt.fmt.pix.bytesperline = 0;
    }
    return ioctl(VIDIOC_TRY_FMT, &fmt);
}

bool VideoDevice::s_fmt(v4l2_format &fmt)
{
    /*Set format*/
    if (V4L2_TYPE_IS_MULTIPLANAR(fmt.type)) {
        fmt.fmt.pix_mp.plane_fmt[0].bytesperline = 0;
        fmt.fmt.pix_mp.plane_fmt[1].bytesperline = 0;
    } else {
        fmt.fmt.pix.bytesperline = 0;
    }
    return ioctl(VIDIOC_S_FMT, &fmt);
}

bool VideoDevice::reqbufs(v4l2_requestbuffers &req_buf, __u32 buf_type, int count)
{
    memset(&req_buf, 0, sizeof(req_buf));
    req_buf.type = buf_type;
    req_buf.memory = V4L2_MEMORY_MMAP;
    req_buf.count = count;

    return ioctl(VIDIOC_REQBUFS, &req_buf);
}

bool VideoDevice::get_frame()
{
    struct pollfd fds;
    int r = 0;

    /* Setting fds */
    fds.fd = fd;
    fds.events = POLLIN;

    struct v4l2_buffer buf;
    memset(&buf, 0, sizeof(buf));
    buf.type = buf_type;
    buf.memory = V4L2_MEMORY_MMAP;

    while(true){

        /*Wait for data to be input*/
        r = poll(&fds, 1, 1*1000);

        switch (r) {
        case -1:
            error_msg = "Poll function error";
            std::cerr<<error_msg<<std::endl;
            return false;

        case 0:
            error_msg = "poll function timeout(1ms)";
            std::cerr<<error_msg<<std::endl;
            return false;

        default:
            /*Get new frame data*/
            if(!ioctl(VIDIOC_DQBUF, &buf))
                return false;

            assert(buf.index < buf_cnt);
            memcpy(frame_data,  buffers[buf.index].data, src_fmt.fmt.pix.sizeimage);

            /*Queue buffer*/
            if(!ioctl(VIDIOC_QBUF, &buf))
                return false;
        }

        return true;
    }
}

uint VideoDevice::convert_format(void* dst_data, __u32 dst_format)
{
    dst_fmt.fmt.pix.pixelformat = dst_format;
    int len = src_fmt.fmt.pix.width * src_fmt.fmt.pix.height;

    switch (dst_format){
    case V4L2_PIX_FMT_BGR24:
        dst_fmt.fmt.pix.sizeimage = len*3;break;
    case V4L2_PIX_FMT_RGB24:
        dst_fmt.fmt.pix.sizeimage = len*3;break;
    case V4L2_PIX_FMT_GREY:
        dst_fmt.fmt.pix.sizeimage = len;break;
    }

    if(src_fmt.fmt.pix.pixelformat != dst_format){

        /*Convert pixel format*/
        if(v4lconvert_convert(convert_data, &src_fmt, &dst_fmt,
                              (unsigned char*)frame_data,
                              src_fmt.fmt.pix.sizeimage,
                              (unsigned char*)dst_data,
                              dst_fmt.fmt.pix.sizeimage) == -1)
            return 0;
    }else{
        memcpy(dst_data, frame_data, dst_fmt.fmt.pix.sizeimage);
    }

    return dst_fmt.fmt.pix.sizeimage;
}

bool VideoDevice::enum_fmt(v4l2_fmtdesc &fmt, unsigned type, int index)
{
    memset(&fmt, 0, sizeof(fmt));
    fmt.index = index;
    fmt.type = type;

    return ioctl(VIDIOC_ENUM_FMT, &fmt, false);
}

std::string VideoDevice::pixfmt_to_str(__u32 id)
{
    std::string pixfmt;
    pixfmt.clear();

    pixfmt = (char)(id & 0xff);
    pixfmt += (char)((id >> 8) & 0xff);
    pixfmt += (char)((id >> 16) & 0xff);
    pixfmt += (char)((id >> 24) & 0xff);

    return pixfmt;
}

std::string VideoDevice::buftype_to_str(__u32 id)
{
    std::string type;

    switch(id){
    case V4L2_BUF_TYPE_VIDEO_CAPTURE :
        type = "VIDEO_CAPTURE";break;
    case V4L2_BUF_TYPE_VIDEO_OUTPUT :
        type = "VIDEO_OUTPUT";break;
    case V4L2_BUF_TYPE_VIDEO_OVERLAY :
        type = "VIDEO_OVERLAY";break;
    case V4L2_BUF_TYPE_VBI_CAPTURE :
        type = "VBI_CAPTURE";break;
    case V4L2_BUF_TYPE_VBI_OUTPUT :
        type = "VBI_OUTPUT";break;
    case V4L2_BUF_TYPE_SLICED_VBI_CAPTURE :
        type = "SLICED_VBI_CAPTURE";break;
    case V4L2_BUF_TYPE_SLICED_VBI_OUTPUT :
        type = "SLICED_VBI_OUTPUT";break;
    case V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY :
        type = "VIDEO_OUTPUT_OVERLAY";break;
    case V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE :
        type = "VIDEO_CAPTURE_MPLANE";break;
    case V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE :
        type = "VIDEO_OUTPUT_MPLANE";break;
    case V4L2_BUF_TYPE_SDR_CAPTURE :
        type = "SDR_CAPTURE";break;
    case V4L2_BUF_TYPE_SDR_OUTPUT :
        type = "SDR_OUTPUT";break;
    case V4L2_BUF_TYPE_META_CAPTURE :
        type = "META_CAPTURE";break;
    case V4L2_BUF_TYPE_PRIVATE :
        type = "PRIVATE";break;
    }

    return type;
}
