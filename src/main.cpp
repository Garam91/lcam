#include <iostream>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char* argv[])
{
    std::cout<<"LCam(Linux Camera)"<<std::endl;

    QApplication app(argc, argv);
    MainWindow main_window;
    main_window.show();

    return app.exec();
}
