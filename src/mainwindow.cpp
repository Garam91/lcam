#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>
#include <unistd.h>
#include <ctime>
#include <QFileDialog>

void current_time(QString& cur_time)
{
    time_t timer;
    struct tm* t;
    timer = time(NULL); // 1970년 1월 1일 0시 0분 0초부터 시작하여 현재까지의 초
    t = localtime(&timer); // 포맷팅을 위해 구조체에 넣기

    cur_time.clear();
    cur_time.sprintf("%d%0.2d%0.2d %0.2d:%0.2d:%0.2d", t->tm_year + 1900, t->tm_mon + 1,
                     t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), preview(false), fps(0),
    img_save(false), video_save(false)
{
    ui->setupUi(this);

    spinbox_map.clear();
    checkbox_map.clear();
    spinbox_map[V4L2_CID_BRIGHTNESS] = ui->spinBox_brightness;
    spinbox_map[V4L2_CID_CONTRAST] = ui->spinBox_constrast;
    spinbox_map[V4L2_CID_EXPOSURE_ABSOLUTE] = ui->spinBox_ex;
    spinbox_map[V4L2_CID_FOCUS_ABSOLUTE] = ui->spinBox_focus;
    spinbox_map[V4L2_CID_GAIN] = ui->spinBox_gain;
    spinbox_map[V4L2_CID_GAMMA] = ui->spinBox_gamma;
    spinbox_map[V4L2_CID_HUE] = ui->spinBox_hue;
    spinbox_map[V4L2_CID_PAN_ABSOLUTE] = ui->spinBox_pan;
    spinbox_map[V4L2_CID_SATURATION] = ui->spinBox_saturation;
    spinbox_map[V4L2_CID_SHARPNESS] = ui->spinBox_sharpness;
    spinbox_map[V4L2_CID_TILT_ABSOLUTE] = ui->spinBox_tilt;
    spinbox_map[V4L2_CID_WHITE_BALANCE_TEMPERATURE] = ui->spinBox_wb;
    spinbox_map[V4L2_CID_ZOOM_ABSOLUTE] = ui->spinBox_zoom;
    checkbox_map[V4L2_CID_EXPOSURE_AUTO_PRIORITY] = ui->checkBox_auto_ex_priority;
    checkbox_map[V4L2_CID_EXPOSURE_AUTO] = ui->checkBox_auto_ex;
    checkbox_map[V4L2_CID_AUTO_WHITE_BALANCE] = ui->checkBox_auto_wb;
    checkbox_map[V4L2_CID_HFLIP] = ui->checkBox_h_flip;
    checkbox_map[V4L2_CID_VFLIP] = ui->checkBox_v_flip;

    connect(this, SIGNAL(frame_capture()), this, SLOT(frame_update()));

    /*Create thread*/
    preview_thread = new std::thread([&](){Capture();});
}

void MainWindow::SettingWidget_SetEnabled(bool enable)
{
    ui->comboBox_resolution->setEnabled(enable);
    ui->comboBox_color_space->setEnabled(enable);
    ui->spinBox_brightness->setEnabled(enable);
    ui->spinBox_constrast->setEnabled(enable);
    ui->spinBox_ex->setEnabled(enable);
    ui->spinBox_focus->setEnabled(enable);
    ui->spinBox_gain->setEnabled(enable);
    ui->spinBox_gamma->setEnabled(enable);
    ui->spinBox_hue->setEnabled(enable);
    ui->spinBox_pan->setEnabled(enable);
    ui->spinBox_saturation->setEnabled(enable);
    ui->spinBox_sharpness->setEnabled(enable);
    ui->spinBox_tilt->setEnabled(enable);
    ui->spinBox_wb->setEnabled(enable);
    ui->spinBox_zoom->setEnabled(enable);
    ui->checkBox_auto_ex_priority->setEnabled(enable);
    ui->checkBox_auto_ex->setEnabled(enable);
    ui->checkBox_auto_wb->setEnabled(enable);
    ui->checkBox_h_flip->setEnabled(enable);
    ui->checkBox_v_flip->setEnabled(enable);
}

MainWindow::~MainWindow()
{
    delete preview_thread;
    delete ui;
}

void MainWindow::showEvent(QShowEvent *event)
{
    /*Get current connected device list*/
    QString dn;
    int num = VideoDevice::GetDeviceList(cam_list);

    if(num > 0){
        ui->pushButton_open->setEnabled(true);
    }else if(num == 0)
        ui->statusbar->showMessage("No surch device");

    /*Display list*/
    for(int i = 0 ; i < cam_list.size() ; i++){
        dn = QString::fromLocal8Bit((const char*)cam_list[i].cap.card);
        ui->comboBox_camera_list->addItem(dn);
    }

    QWidget::showEvent(event);
}

void MainWindow::on_pushButton_open_clicked()
{
    int i;
    int index = ui->comboBox_camera_list->currentIndex();

    /*Camera open*/
    if(cam.Open(cam_list[index].name.c_str())){
        /*Get camera information*/
        cam.GetFormatList();
        cam.GetResolutionList();
        cam.GetControlsList();

        /*Camera initialization*/
        cam.SetFormat(cam.format_list[0].fmt_desc.pixelformat,
                      cam.frame_size_list[0].width,
                      cam.frame_size_list[0].height);
//        cam.SetRsolution(cam.frame_size_list[0].width,
//                         cam.frame_size_list[0].height);
        cam.PrepareBuffer(6);
        cam.StreamOn();

        /*Get camera information*/
        width = cam.GetWidth();
        height = cam.GetHeight();
        device_name = cam.GetName();
        pix_fmt = cam.GetFormat();
        fps = cam.GetFps();

        /*Start preview thread*/
        preview = false;
        preview_con.notify_one();

        /*Set UI*/
        ui->pushButton_open->setEnabled(false);
        ui->pushButton_close->setEnabled(true);
        ui->comboBox_resolution->setEnabled(true);
        ui->comboBox_color_space->setEnabled(true);
        ui->widget_image_video->setEnabled(true);
        ui->comboBox_resolution->clear();
        ui->comboBox_color_space->clear();

        QString res;
        QString fmt;
        QSpinBox* spin_box;
        QCheckBox* check_box;
        for(i = 0 ; i < cam.frame_size_list.size() ; i++){
            res = QString("%1 x %2").arg(cam.frame_size_list[i].width).
                    arg(cam.frame_size_list[i].height);
            ui->comboBox_resolution->addItem(res);
        }

        for(i = 0 ; i < cam.format_list.size() ; i++){
            fmt = (char*)cam.format_list[i].fmt_desc.description;
            ui->comboBox_color_space->addItem(fmt);
        }

        /*Setting value*/
        try{
            for(i = 0 ; i < cam.control_list.size() ; i++){
                switch(cam.control_list[i].type){
                case V4L2_CTRL_TYPE_INTEGER:
                    break;
                case V4L2_CTRL_TYPE_BOOLEAN:
                    check_box = checkbox_map.at(cam.control_list[i].id);
                    check_box->setChecked(cam.control_list[i].default_value);
                    check_box->setEnabled(true);
                    break;
                }
            }

            for(i = 0 ; i < cam.control_list.size() ; i++){
                switch(cam.control_list[i].type){
                case V4L2_CTRL_TYPE_INTEGER:
                    spin_box = spinbox_map.at(cam.control_list[i].id);
                    spin_box->setMinimum(cam.control_list[i].minimum);
                    spin_box->setMaximum(cam.control_list[i].maximum);
                    spin_box->setValue(cam.control_list[i].default_value);

                    if((cam.control_list[i].id == V4L2_CID_EXPOSURE_ABSOLUTE) ||
                       (cam.control_list[i].id == V4L2_CID_WHITE_BALANCE_TEMPERATURE))
                        spin_box->setEnabled(!cam.control_list[i].default_value);
                    else
                        spin_box->setEnabled(true);
                    break;
                case V4L2_CTRL_TYPE_BOOLEAN:
                    break;
                }
            }
        }catch(std::exception& e){
            std::cerr<<cam.control_list[i].name<<" cannot be set"<<std::endl;
        }

        ui->lineEdit_path->setText(QDir::currentPath());
    }
}

void MainWindow::on_pushButton_close_clicked()
{
    preview = true;
    ui->statusbar->showMessage("close");

    ui->pushButton_open->setEnabled(true);
    ui->pushButton_close->setEnabled(false);
    ui->widget_image_video->setEnabled(false);
    SettingWidget_SetEnabled(false);
}

void MainWindow::Capture()
{
    std::unique_lock<std::mutex> lock(preview_mutex);

    uchar* frame_data;
    QTime timer;

    do{
        preview_con.wait(lock);

        /*Frame memory*/
        frame_qimg = new QImage(width, height, QImage::Format_RGB888);//RGB format
        frame_mat = new cv::Mat(height, width, CV_8UC3, frame_qimg->bits());
        frame_data = frame_qimg->bits();

        do{
            timer.start();

            /*Capture*/
            cam.Capture(frame_data);

            /*Real-time fps measurement*/
            period = timer.elapsed();
            emit frame_capture();
        }while(!preview);

        ui->label_frame->clear();

        cam.StreamOff();
        cam.FreeBuffer();
        cam.Close();

        delete frame_qimg;
        delete frame_mat;
    }while(true);
}

void MainWindow::frame_update()
{
    QPixmap org_frame_pixmap;
    QPixmap resize_frame_pixmap;

    if(img_save){
        QString t;
        QString extension = ui->comboBox_img_extension->currentText();
        QString path = ui->lineEdit_path->text();

        current_time(t);
        frame_qimg->save(path + "/" + t + extension);
        img_save = false;
    }

    if(video_save){//Save video
        cv::Mat bgr;
        cv::cvtColor(*frame_mat, bgr, CV_RGB2BGR);
        video_writer << bgr;
    }else if((!video_save) && video_writer.isOpened()){//close video file
        video_writer.release();
    }

    /*Display frame image*/
    org_frame_pixmap = QPixmap::fromImage(*frame_qimg);
    resize_frame_pixmap = org_frame_pixmap.scaled(
                ui->label_frame->width(), ui->label_frame->height());//resize
    ui->label_frame->setPixmap(resize_frame_pixmap);

    /*Print fps*/
    int fps = 1000/period;
    ui->statusbar->showMessage(
                QString("%1 open(%2 x %3 %4), fps : %5").arg(device_name.c_str()).
                arg(width).arg(height).arg(pix_fmt.c_str()).arg(fps));
}

void MainWindow::on_pushButton_save_img_clicked()
{
    img_save = true;
}

void MainWindow::on_pushButton_record_start_clicked()
{
    QString t;
    int fourcc;
    QString extension = ui->comboBox_video_extension->currentText();
    QString path = ui->lineEdit_path->text();
    current_time(t);

    QString fn = path + "/" + t + extension;

    switch(ui->comboBox_video_extension->currentIndex()){
    case 0:
        fourcc = cv::VideoWriter::fourcc('H', '2', '6', '4');
        break;

    case 1:
        fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');

    case 2:
        fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');
        break;

    case 3:
        fourcc = cv::VideoWriter::fourcc('H', '2', '6', '4');
        break;
    }

    video_writer.open(fn.toStdString().c_str(), fourcc,
                      (float)fps, cv::Size(width, height));

    video_save = true;
}

void MainWindow::on_pushButton_record_stop_clicked()
{
    video_save = false;
}

void MainWindow::on_comboBox_resolution_activated(int index)
{
    preview = true;
    sleep(1);

    int cam_index = ui->comboBox_camera_list->currentIndex();
    int fmt_index = ui->comboBox_color_space->currentIndex();
    if(cam.Open(cam_list[cam_index].name.c_str())){
        /*Camera initialization*/
        cam.SetFormat(cam.format_list[fmt_index].fmt_desc.pixelformat,
                      cam.frame_size_list[index].width,
                      cam.frame_size_list[index].height);
        cam.PrepareBuffer();
        cam.StreamOn();

        width = cam.GetWidth();
        height = cam.GetHeight();

        /*Start preview thread*/
        preview = false;
        preview_con.notify_one();
    }
}

void MainWindow::on_comboBox_color_space_activated(int index)
{
    preview = true;
    sleep(1);

    int cam_index = ui->comboBox_resolution->currentIndex();
    int resolution_index = ui->comboBox_resolution->currentIndex();
    if(cam.Open(cam_list[cam_index].name.c_str())){
        /*Camera initialization*/
        cam.SetFormat(cam.format_list[index].fmt_desc.pixelformat,
                      cam.frame_size_list[resolution_index].width,
                      cam.frame_size_list[resolution_index].height);
        cam.PrepareBuffer();
        cam.StreamOn();

        width = cam.GetWidth();
        height = cam.GetHeight();
        fps = cam.GetFps();

        /*Start preview thread*/
        preview = false;
        preview_con.notify_one();
    }
}

void MainWindow::on_spinBox_brightness_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_BRIGHTNESS, arg1);
}

void MainWindow::on_spinBox_constrast_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_CONTRAST, arg1);
}

void MainWindow::on_checkBox_auto_ex_priority_toggled(bool checked)
{
    cam.SetControl(V4L2_CID_EXPOSURE_AUTO_PRIORITY, checked);
}

void MainWindow::on_checkBox_auto_ex_toggled(bool checked)
{
    int set = cam.SetControl(V4L2_CID_EXPOSURE_AUTO, checked);
    bool enable;

    if(set && checked)
        enable = false;
    else if(set && (checked == false))
        enable = true;

    ui->spinBox_ex->setEnabled(enable);
}

void MainWindow::on_spinBox_ex_valueChanged(int arg1)
{
    if(!ui->checkBox_auto_ex->isChecked())
        cam.SetControl(V4L2_CID_EXPOSURE_ABSOLUTE, arg1);
}

void MainWindow::on_spinBox_focus_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_FOCUS_ABSOLUTE, arg1);
}

void MainWindow::on_spinBox_gain_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_GAIN, arg1);
}

void MainWindow::on_spinBox_gamma_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_GAMMA, arg1);
}

void MainWindow::on_spinBox_hue_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_HUE, arg1);
}

void MainWindow::on_spinBox_pan_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_PAN_ABSOLUTE, arg1);
}

void MainWindow::on_spinBox_saturation_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_SATURATION, arg1);
}

void MainWindow::on_spinBox_sharpness_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_SHARPNESS, arg1);
}

void MainWindow::on_spinBox_tilt_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_TILT_ABSOLUTE, arg1);
}

void MainWindow::on_checkBox_auto_wb_toggled(bool checked)
{
    int set = cam.SetControl(V4L2_CID_AUTO_WHITE_BALANCE, checked);
    bool enable;

    if(set && checked)
        enable = false;
    else if(set && (checked == false))
        enable = true;

    ui->spinBox_wb->setEnabled(enable);
}

void MainWindow::on_spinBox_wb_valueChanged(int arg1)
{
    if(!ui->checkBox_auto_wb->isChecked())
        cam.SetControl(V4L2_CID_WHITE_BALANCE_TEMPERATURE, arg1);
}

void MainWindow::on_spinBox_zoom_valueChanged(int arg1)
{
    cam.SetControl(V4L2_CID_ZOOM_ABSOLUTE, arg1);
}

void MainWindow::on_pushButton_browse_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this,
                                                     "", QDir::currentPath());
    ui->lineEdit_path->setText(path);
}
