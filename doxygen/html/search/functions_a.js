var searchData=
[
  ['s_5ffmt_185',['s_fmt',['../class_video_device.html#a7b87e637f0cce2eeecc7080380ab76f3',1,'VideoDevice']]],
  ['setcontrol_186',['SetControl',['../class_video_device.html#ad3f5d63607bbd8022818882684c66725',1,'VideoDevice']]],
  ['setformat_187',['SetFormat',['../class_video_device.html#a999d95db95aebd635833db16d0d7af53',1,'VideoDevice::SetFormat()'],['../class_video_device.html#a05bbca8c9b015e3d48b8898ffdf402be',1,'VideoDevice::SetFormat(__u32 pixel_fmt)'],['../class_video_device.html#a303b6874fcef74b108fcab61dec5be98',1,'VideoDevice::SetFormat(__u32 pixel_fmt, __u32 w, __u32 h)']]],
  ['setrsolution_188',['SetRsolution',['../class_video_device.html#a0823ab8e48f498ec2e34f64841d844d4',1,'VideoDevice']]],
  ['settingwidget_5fsetenabled_189',['SettingWidget_SetEnabled',['../class_main_window.html#ae5fa4f82b361264e7eb78549c3f57f34',1,'MainWindow']]],
  ['showevent_190',['showEvent',['../class_main_window.html#aad878d0e4da902c98db27e222dfa2226',1,'MainWindow']]],
  ['streamoff_191',['StreamOff',['../class_video_device.html#a39faa59cf2510ce0012e236f6f27d946',1,'VideoDevice']]],
  ['streamon_192',['StreamOn',['../class_video_device.html#ab9a2aa4446183c42a4d5f5a4076a278b',1,'VideoDevice']]]
];
