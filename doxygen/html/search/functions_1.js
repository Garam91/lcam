var searchData=
[
  ['capture_132',['Capture',['../class_main_window.html#a105c789aa301b996d4dc9c07a3eff74e',1,'MainWindow::Capture()'],['../class_video_device.html#a5373ce4f5c3d714d25325eddae2f4806',1,'VideoDevice::Capture(unsigned char *data, __u32 format=V4L2_PIX_FMT_RGB24)']]],
  ['close_133',['Close',['../class_video_device.html#a7f7a3199c392465d0767c6506c1af5b4',1,'VideoDevice']]],
  ['close_5fdevice_134',['close_device',['../class_video_device.html#a207d247b175aba8213284ecf344ddd77',1,'VideoDevice']]],
  ['convert_5fformat_135',['convert_format',['../class_video_device.html#afe634bf50f97ccadc5769c54e2103560',1,'VideoDevice']]],
  ['current_5ftime_136',['current_time',['../mainwindow_8cpp.html#aeba2f38d0d13a7cf853a25b97b5c0b11',1,'mainwindow.cpp']]]
];
