var searchData=
[
  ['s_5ffmt_98',['s_fmt',['../class_video_device.html#a7b87e637f0cce2eeecc7080380ab76f3',1,'VideoDevice']]],
  ['setcontrol_99',['SetControl',['../class_video_device.html#ad3f5d63607bbd8022818882684c66725',1,'VideoDevice']]],
  ['setformat_100',['SetFormat',['../class_video_device.html#a999d95db95aebd635833db16d0d7af53',1,'VideoDevice::SetFormat()'],['../class_video_device.html#a05bbca8c9b015e3d48b8898ffdf402be',1,'VideoDevice::SetFormat(__u32 pixel_fmt)'],['../class_video_device.html#a303b6874fcef74b108fcab61dec5be98',1,'VideoDevice::SetFormat(__u32 pixel_fmt, __u32 w, __u32 h)']]],
  ['setrsolution_101',['SetRsolution',['../class_video_device.html#a0823ab8e48f498ec2e34f64841d844d4',1,'VideoDevice']]],
  ['settingwidget_5fsetenabled_102',['SettingWidget_SetEnabled',['../class_main_window.html#ae5fa4f82b361264e7eb78549c3f57f34',1,'MainWindow']]],
  ['showevent_103',['showEvent',['../class_main_window.html#aad878d0e4da902c98db27e222dfa2226',1,'MainWindow']]],
  ['size_104',['size',['../struct__buffer.html#a854352f53b148adc24983a58a1866d66',1,'_buffer']]],
  ['spinbox_5fmap_105',['spinbox_map',['../class_main_window.html#a4d682c1377e550bca8e26d7c26818b61',1,'MainWindow']]],
  ['src_5ffmt_106',['src_fmt',['../class_video_device.html#af6d25d4a36fbad88fa5cad58eef77945',1,'VideoDevice']]],
  ['streamoff_107',['StreamOff',['../class_video_device.html#a39faa59cf2510ce0012e236f6f27d946',1,'VideoDevice']]],
  ['streamon_108',['StreamOn',['../class_video_device.html#ab9a2aa4446183c42a4d5f5a4076a278b',1,'VideoDevice']]]
];
