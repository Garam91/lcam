var indexSectionsWithContent =
{
  0: "_bcdefghimnoprstuvw~",
  1: "_mv",
  2: "u",
  3: "mv",
  4: "bcefgimoprstv~",
  5: "bcdefhinopsuvw",
  6: "bc",
  7: "b"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines"
};

var indexSectionLabels =
{
  0: "모두",
  1: "클래스",
  2: "네임스페이스들",
  3: "파일들",
  4: "함수",
  5: "변수",
  6: "타입정의",
  7: "매크로"
};

